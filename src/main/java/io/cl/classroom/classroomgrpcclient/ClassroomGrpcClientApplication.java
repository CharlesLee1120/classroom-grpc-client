package io.cl.classroom.classroomgrpcclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassroomGrpcClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClassroomGrpcClientApplication.class, args);
	}

}
